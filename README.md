# README #

This project is intended to provide a simple and a fast way to set up VM with installed and pre-configured apache webserver, PHP7 and MariaDB. This is helpful to quickly try out the video app or for any other development project. 

### What is needed? ###

What is needed before this box can be used:

* VirtualBox 4.2.16: [https://www.vagrantup.com/downloads.html](Link URL)
* Vagrant 5.1.14 [https://www.virtualbox.org/wiki/Downloads](Link URL)
* Git 2.11.0 [https://git-scm.com/downloads](Link URL)

### Setup ###

You firstly need to clone this repo from BitBucket:
https://scleijnen@bitbucket.org/scleijnen/vagrant_test.git

After the clone of the vagrant environment for example the video symfony app can be cloned into the /src/video_app folder:
https://scleijnen@bitbucket.org/scleijnen/video_app.git

### Video app ###

The demo video app (see other repository under setup) can be used for a example application.
Whithin the video_app composer firstly needs to be runned for getting all dependencies/bundles for this Symfony project.
Therefore it is needed to SSH to the corresponing vagrant environment and run composer before going further.

Phing is used for building up all other things like the database. (see build.xml in video_app)
The following command(s) needs to be executed:

* vendor/phing/phing/bin/phing init:env -Denv=dev
* vendor/phing/phing/bin/phing init:env -Denv=test

Within this build script datafixtures for adding users and videos (fictive) for specific users is included.

After these steps the video app application is ready an can be reached:

* http://192.168.33.10:22202/app_dev.php/login

The following users can be used for logging into the app:
testuser1
testuser1!

or

testuser2
testuser2!

Both have other video's and can only see the files they uploaded by them selve.

### Database ###
The database can be reached with these credentials:

*     database_host:     192.168.33.10
*     database_port:     3306
*     database_name:     video_app
*     database_user:     root
*     database_password: root
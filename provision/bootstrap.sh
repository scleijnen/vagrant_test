#!/usr/bin/env bash

yum install -y httpd
yum install -y mercurial
yum install -y git
yum install -y vim vim-gtk

yum install -y freetds
yum install -y unixODBC
yum install -y net-tools
yum install -y wget
yum install -y mlocate
yum install -y tmux
yum install -y yum-utils
#yum install -y sqsh

#EPEL is necessary for at least PHP 7 and VirtualBox guest additions
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

updatedb

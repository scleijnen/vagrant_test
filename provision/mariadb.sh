yum install -y mariadb mariadb-server
systemctl enable mariadb
systemctl start mariadb

echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;" | mysql -uroot
#echo Run 'sudo mysql_secure_installation' to set up a root password for MariaDB

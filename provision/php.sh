PHP_PREFIX=/opt/remi/php70

wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm

wget http://rpms.remirepo.net/enterprise/remi.repo -O /etc/yum.repos.d/remi.repo

#yum-config-manager -y --enable remi-php71
yum update -y

yum install -y php70

ln -s $PHP_PREFIX/enable /etc/profile.d/enable_remi-php70.sh

yum install -y php70-php
yum install -y php70-php-pear php70-php-bcmath php70-php-pecl-jsond-devel php70-php-gd php70-php-common php70-php-fpm php70-php-intl php70-php-cli php70-php php70-php-xml php70-php-opcache php70-php-pecl-apcu php70-php-pecl-jsond php70-php-pdo php70-php-gmp php70-php-process php70-php-pecl-imagick php70-php-devel php70-php-mbstring php70-php-mcrypt php70-php-odbc php70-php-pdo-dblib php70-php-pecl-zip php70-php-mysqlnd php70-php-pecl-xdebug php70-php-soap.x86_64

#composer
curl -sS https://getcomposer.org/installer | sudo $PHP_PREFIX/root/bin/php -- --install-dir=/usr/local/bin --filename=composer

#sf installer
curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
chmod a+x /usr/local/bin/symfony

#psr-cs-fixer
wget http://get.sensiolabs.org/php-cs-fixer.phar -O php-cs-fixer
chmod +x php-cs-fixer
mv php-cs-fixer /usr/local/bin/php-cs-fixer

#our php.ini has some specific settings
cp /vagrant/provision/php/php.ini /etc$PHP_PREFIX/
cp /vagrant/provision/php/15-xdebug.ini /etc$PHP_PREFIX/php.d/

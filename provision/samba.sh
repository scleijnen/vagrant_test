#Configure Samba
echo "Installing samba..."

yum install -y samba

echo -ne "vagrant\nvagrant\n" | smbpasswd -a vagrant

mv /etc/samba/smb.conf /etc/samba/smb.conf.bkp
cp /vagrant/provision/samba/smb.conf /etc/samba/smb.conf

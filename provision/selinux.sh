#Configure HTTPD
#Configure API

echo "Disabling SELinux"
sed -i s/SELINUX=enforcing/SELINUX=disabled/g /etc/sysconfig/selinux

echo "Manual reboot is required : vagrant halt && vagrant up"

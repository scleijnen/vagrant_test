#Starting HTTPD
echo "Starting HTTPD"

service httpd restart

echo "Starting MariaDB"
service mariadb start

echo "Starting Samba"
service smb start

setenforce 0

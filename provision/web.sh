#Configure HTTPD
echo "Updating httpd.conf file..."
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bkp
cp /vagrant/provision/httpd/httpd.conf /etc/httpd/conf/httpd.conf

#Setting SELinux policy for httpd
setsebool -P httpd_use_nfs 1
semanage port -a -t http_port_t -p tcp 22202

#Configure video_app
echo "Creating sites-enabled folder..."
mkdir /etc/httpd/sites-available /etc/httpd/sites-enabled

echo "Copying video_app.conf..."
cp /vagrant/provision/httpd/video_app.conf /etc/httpd/sites-available
ln -s /etc/httpd/sites-available/video_app.conf /etc/httpd/sites-enabled

service httpd restart

echo "Manual step (optional): add entry to your host file : IP video_app.assessment.local"
echo "Manual step (optional): add entry to guest file : 127.0.0.1 video_app.assessment.local"
